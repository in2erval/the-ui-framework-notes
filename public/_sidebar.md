[Overview](/overview.md)

**Setup and installation**

- [Buildless deployment](/setup-install/buildless/)
- [Build tools and compilation](/setup-install/build-tools/)
- [Scaffolding](/setup-install/scaffolding/)
- [CLI](/setup-install/cli/)
- [Developer tools](/setup-install/dev-tools/)
- [Typescript](/setup-install/typescript/)

**Basic usage**

- [Entrypoint](/basic/entry/)
- [Components](/basic/components/)
- [Reactivity](/basic/reactivity/)
- [Control structures](/basic/control-structures/)
- [Top-down data flow](/basic/top-down/)
- [Bottom-up data flow](/basic/bottom-up/)
- [Events](/basic/events/)
- [Lifecycle](/basic/lifecycle/)
- [DOM references](/basic/dom-refs/)

**Advanced usage**

- [Component caching](/advanced/caching/)
- [Portals](/advanced/portals/)
- [Suspense](/advanced/suspense/)
- [Unwrapped components](/advanced/unwrapped/)
- [Error handling](/advanced/error-handling/)
- [Drag-and-Drop](/advanced/dnd/)
- [Touchscreens](/advanced/touchscreens/)

**Shared data**

- [Data contexts](/shared-data/contexts/)
- [Data stores](/shared-data/stores/)
- [Globals](/shared-data/globals/)
- [Browser stores](/shared-data/browser/)

**Styling**

- [Static styles](/styling/static/)
- [Dynamic styles](/styling/dynamic/)
- [Utility-based CSS](/styling/utility/)
- [Theming](/styling/theming/)
- [Animation](/styling/animation/)

**User data**

- [Forms](/user-data/forms/)
- [Charts and diagrams](/user-data/charts-diagrams/)
- [Datagrids](/user-data/grids/)

**Routing**

- [Component switching](/routing/switch/)
- [Passing data](/routing/passing-data/)
- [Nested routes](/routing/nested/)
- [Catch-all](/routing/catch-all/)
- [Transitions](/routing/transitions/)

**Networking**

- [HTTP](/networking/http/)
- [GraphQL](/networking/graphql/)
- [Other networking](/networking/others/)

**Auxiliary activities**

- [Testing](/auxiliary/testing/)
- [Documentation](/auxiliary/documentation/)
- [Internationalisation](/auxiliary/i18n/)
- [Browser services](/auxiliary/browser-services/)

**Deployment patterns**

- [SSR / SSG](/deployment/ssr-ssg/)
- [Mobile / Native](/deployment/mobile/)
- [Web Components](/deployment/web-components/)
- [Legacy browsers](/deployment/legacy/)
- [Micro-frontends](/deployment/mfe/)

**Programming paradigms**

- [Reactive programming](/paradigms/reactive/)
- [Finite state machines](/paradigms/fsm/)

**Interop**

- [Other UI frameworks](/interop/other-frameworks/)
- [DOM-based libraries](/interop/dom-based/)
- [General libraries](/interop/general/)

**Non-functional aspects**

- [Accessibility](/non-functional/a11y/)
- [Security](/non-functional/security/)
- [Runtime performance](/non-functional/performance/)
- [Scaling](/non-functional/scaling/)
- [Refactoring](/non-functional/refactoring/)
- [Transitioning](/non-functional/transitioning/)
