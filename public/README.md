# The Web UI Framework Notes

## A knowledge dump of JS-based web UI frameworks

<div class="frameworks-list">
  <a href="https://react.dev" target="_blank" rel="noopener noreferrer">
    <img style="width: 128px;" src="_assets/images/react.svg" data-origin="_assets/images/react.svg" alt="React">
  </a>
  <a href="https://vuejs.org" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/vue-js.svg" data-origin="_assets/images/vue-js.svg" alt="Vue.js">
  </a>
  <a href="https://angular.io" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/angular.svg" data-origin="_assets/images/angular.svg" alt="Angular">
  </a>
  <a href="https://svelte.dev" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/svelte.svg" data-origin="_assets/images/svelte.svg" alt="Svelte">
  </a>
  <a href="https://preactjs.com" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/preact.svg" data-origin="_assets/images/preact.svg" alt="Preact">
  </a>
  <a href="https://www.solidjs.com" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/solid-js.svg" data-origin="_assets/images/solid-js.svg" alt="Solid.js">
  </a>
  <a href="https://alpinejs.dev" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/alpine-js.svg" data-origin="_assets/images/alpine-js.svg" alt="Alpine.js">
  </a>
  <a href="https://aurelia.io" target="_blank" rel="noopener noreferrer" title="(Future content)">
    <img style="width: 128px; filter: grayscale(100%); opacity: 0.5" src="_assets/images/aurelia.svg" data-origin="_assets/images/aurelia.svg" alt="Aurelia">
  </a>
</div>

<div class="links-list">
  <a class="card" href="#/overview">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>text-box-outline</title><path d="M5,3C3.89,3 3,3.89 3,5V19C3,20.11 3.89,21 5,21H19C20.11,21 21,20.11 21,19V5C21,3.89 20.11,3 19,3H5M5,5H19V19H5V5M7,7V9H17V7H7M7,11V13H17V11H7M7,15V17H14V15H7Z" /></svg>
    <h1 class="title">Overview</h1>
    <p class="subtitle">A brief explanation of this docsite</p>
  </a>
  <a class="card" href="#/setup-install/buildless/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>package-variant</title><path d="M2,10.96C1.5,10.68 1.35,10.07 1.63,9.59L3.13,7C3.24,6.8 3.41,6.66 3.6,6.58L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.66,6.72 20.82,6.88 20.91,7.08L22.36,9.6C22.64,10.08 22.47,10.69 22,10.96L21,11.54V16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V10.96C2.7,11.13 2.32,11.14 2,10.96M12,4.15V4.15L12,10.85V10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V12.69L14,15.59C13.67,15.77 13.3,15.76 13,15.6V19.29L19,15.91M13.85,13.36L20.13,9.73L19.55,8.72L13.27,12.35L13.85,13.36Z" /></svg>
    <h1 class="title">Setup and installation</h1>
    <p class="subtitle">Getting started with the frameworks</p>
  </a>
  <a class="card" href="#/basic/entry/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>cube-outline</title><path d="M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L6.04,7.5L12,10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V9.21L13,12.58V19.29L19,15.91Z" /></svg>
    <h1 class="title">Basic usage</h1>
    <p class="subtitle">Core concepts and component creation</p>
  </a>
  <a class="card" href="#/advanced/caching/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>shape-plus-outline</title><path d="M11 11V2H2V11M4 9V4H9V9M20 6.5C20 7.9 18.9 9 17.5 9S15 7.9 15 6.5 16.11 4 17.5 4 20 5.11 20 6.5M6.5 14L2 22H11M7.58 20H5.42L6.5 18.08M22 6.5C22 4 20 2 17.5 2S13 4 13 6.5 15 11 17.5 11 22 9 22 6.5M19 17V14H17V17H14V19H17V22H19V19H22V17Z" /></svg>
    <h1 class="title">Advanced usage</h1>
    <p class="subtitle">Complex component programming and special cases</p>
  </a>
  <a class="card" href="#/shared-data/contexts/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>file-tree-outline</title><path d="M12 13H7V18H12V20H5V10H7V11H12V13M8 4V6H4V4H8M10 2H2V8H10V2M20 11V13H16V11H20M22 9H14V15H22V9M20 18V20H16V18H20M22 16H14V22H22V16Z" /></svg>
    <h1 class="title">Shared data</h1>
    <p class="subtitle">Handling data that gets access from multiple places</p>
  </a>
  <a class="card" href="#/styling/static/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>palette-outline</title><path d="M12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2C17.5,2 22,6 22,11A6,6 0 0,1 16,17H14.2C13.9,17 13.7,17.2 13.7,17.5C13.7,17.6 13.8,17.7 13.8,17.8C14.2,18.3 14.4,18.9 14.4,19.5C14.5,20.9 13.4,22 12,22M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C12.3,20 12.5,19.8 12.5,19.5C12.5,19.3 12.4,19.2 12.4,19.1C12,18.6 11.8,18.1 11.8,17.5C11.8,16.1 12.9,15 14.3,15H16A4,4 0 0,0 20,11C20,7.1 16.4,4 12,4M6.5,10C7.3,10 8,10.7 8,11.5C8,12.3 7.3,13 6.5,13C5.7,13 5,12.3 5,11.5C5,10.7 5.7,10 6.5,10M9.5,6C10.3,6 11,6.7 11,7.5C11,8.3 10.3,9 9.5,9C8.7,9 8,8.3 8,7.5C8,6.7 8.7,6 9.5,6M14.5,6C15.3,6 16,6.7 16,7.5C16,8.3 15.3,9 14.5,9C13.7,9 13,8.3 13,7.5C13,6.7 13.7,6 14.5,6M17.5,10C18.3,10 19,10.7 19,11.5C19,12.3 18.3,13 17.5,13C16.7,13 16,12.3 16,11.5C16,10.7 16.7,10 17.5,10Z" /></svg>
    <h1 class="title">Styling</h1>
    <p class="subtitle">Managing component and global CSS</p>
  </a>
  <a class="card" href="#/user-data/forms/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>chart-bar</title><path d="M22,21H2V3H4V19H6V10H10V19H12V6H16V19H18V14H22V21Z" /></svg>
    <h1 class="title">User data</h1>
    <p class="subtitle">Use cases for displaying and handling user-centric data</p>
  </a>
  <a class="card" href="#/routing/switch/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>router</title><path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2M12 20C7.58 20 4 16.42 4 12C4 7.58 7.58 4 12 4C16.42 4 20 7.58 20 12C20 16.42 16.42 20 12 20M13 13V16H15L12 19L9 16H11V13M5 13H8V15L11 12L8 9V11H5M11 11V8H9L12 5L15 8H13V11M19 11H16V9L13 12L16 15V13H19" /></svg>
    <h1 class="title">Routing</h1>
    <p class="subtitle">Handling browser URL changes within the frameworks</p>
  </a>
  <a class="card" href="#/networking/http/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>web</title><path d="M16.36,14C16.44,13.34 16.5,12.68 16.5,12C16.5,11.32 16.44,10.66 16.36,10H19.74C19.9,10.64 20,11.31 20,12C20,12.69 19.9,13.36 19.74,14M14.59,19.56C15.19,18.45 15.65,17.25 15.97,16H18.92C17.96,17.65 16.43,18.93 14.59,19.56M14.34,14H9.66C9.56,13.34 9.5,12.68 9.5,12C9.5,11.32 9.56,10.65 9.66,10H14.34C14.43,10.65 14.5,11.32 14.5,12C14.5,12.68 14.43,13.34 14.34,14M12,19.96C11.17,18.76 10.5,17.43 10.09,16H13.91C13.5,17.43 12.83,18.76 12,19.96M8,8H5.08C6.03,6.34 7.57,5.06 9.4,4.44C8.8,5.55 8.35,6.75 8,8M5.08,16H8C8.35,17.25 8.8,18.45 9.4,19.56C7.57,18.93 6.03,17.65 5.08,16M4.26,14C4.1,13.36 4,12.69 4,12C4,11.31 4.1,10.64 4.26,10H7.64C7.56,10.66 7.5,11.32 7.5,12C7.5,12.68 7.56,13.34 7.64,14M12,4.03C12.83,5.23 13.5,6.57 13.91,8H10.09C10.5,6.57 11.17,5.23 12,4.03M18.92,8H15.97C15.65,6.75 15.19,5.55 14.59,4.44C16.43,5.07 17.96,6.34 18.92,8M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" /></svg>
    <h1 class="title">Networking</h1>
    <p class="subtitle">Patterns for making network requests to APIs and resources</p>
  </a>
  <a class="card" href="#/auxiliary/testing/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>test-tube</title><path d="M7,2V4H8V18A4,4 0 0,0 12,22A4,4 0 0,0 16,18V4H17V2H7M11,16C10.4,16 10,15.6 10,15C10,14.4 10.4,14 11,14C11.6,14 12,14.4 12,15C12,15.6 11.6,16 11,16M13,12C12.4,12 12,11.6 12,11C12,10.4 12.4,10 13,10C13.6,10 14,10.4 14,11C14,11.6 13.6,12 13,12M14,7H10V4H14V7Z" /></svg>
    <h1 class="title">Auxiliary activities</h1>
    <p class="subtitle">Additional aspects surrounding web development</p>
  </a>
  <a class="card" href="#/deployment/ssr-ssg/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>tablet-cellphone</title><path d="M3,4H20A2,2 0 0,1 22,6V8H18V6H5V18H14V20H3A2,2 0 0,1 1,18V6A2,2 0 0,1 3,4M17,10H23A1,1 0 0,1 24,11V21A1,1 0 0,1 23,22H17A1,1 0 0,1 16,21V11A1,1 0 0,1 17,10M18,12V19H22V12H18Z" /></svg>
    <h1 class="title">Deployment patterns</h1>
    <p class="subtitle">Concepts surrounding how the UI gets released</p>
  </a>
  <a class="card" href="#/paradigms/reactive/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>state-machine</title><path d="M6.27 17.05C6.72 17.58 7 18.25 7 19C7 20.66 5.66 22 4 22S1 20.66 1 19 2.34 16 4 16C4.18 16 4.36 16 4.53 16.05L7.6 10.69L5.86 9.7L9.95 8.58L11.07 12.67L9.33 11.68L6.27 17.05M20 16C18.7 16 17.6 16.84 17.18 18H11V16L8 19L11 22V20H17.18C17.6 21.16 18.7 22 20 22C21.66 22 23 20.66 23 19S21.66 16 20 16M12 8C12.18 8 12.36 8 12.53 7.95L15.6 13.31L13.86 14.3L17.95 15.42L19.07 11.33L17.33 12.32L14.27 6.95C14.72 6.42 15 5.75 15 5C15 3.34 13.66 2 12 2S9 3.34 9 5 10.34 8 12 8Z" /></svg>
    <h1 class="title">Programming paradigms</h1>
    <p class="subtitle">Patterns for developing and managing your web application</p>
  </a>
  <a class="card" href="#/interop/other-frameworks/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>pipe</title><path d="M22,14H20V16H14V13H16V11H14V6A2,2 0 0,0 12,4H4V2H2V10H4V8H10V11H8V13H10V18A2,2 0 0,0 12,20H20V22H22" /></svg>
    <h1 class="title">Interop</h1>
    <p class="subtitle">Using the framework alongside other frameworks and libraries</p>
  </a>
  <a class="card" href="#/non-functional/a11y/">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="128px" fill="currentColor"><title>chart-line</title><path d="M16,11.78L20.24,4.45L21.97,5.45L16.74,14.5L10.23,10.75L5.46,19H22V21H2V3H4V17.54L9.5,8L16,11.78Z" /></svg>
    <h1 class="title">Non-functional aspects</h1>
    <p class="subtitle">Qualities of the web application not directly related to functionality</p>
  </a>
</div>
