# Setup

aklsjdf

## easd

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tempus nunc nec ipsum convallis, euismod tristique turpis mattis. Aenean elementum condimentum diam quis hendrerit. Fusce facilisis, tortor id ullamcorper eleifend, mauris metus fringilla turpis, sit amet ultrices diam sem non mi. Phasellus eu sodales mi. Curabitur sed tortor vel elit pharetra ullamcorper eu et eros. Vestibulum id pulvinar quam, nec lobortis sapien. Sed posuere dapibus metus, nec convallis justo imperdiet congue. Ut quis lectus sit amet sem imperdiet cursus. Nam id ultricies sapien. Maecenas vel diam tincidunt dui pretium pellentesque et sodales quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse posuere cursus efficitur. Nulla vestibulum vestibulum tellus. Duis aliquam aliquet fringilla. Praesent et turpis at libero condimentum faucibus vitae ac augue.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam et tortor nec justo congue tempus. Pellentesque fermentum consectetur quam quis pellentesque. Nam tempus ultricies nibh, vitae tristique felis consequat nec. Nam vel finibus ipsum. Integer ornare augue a mauris sodales semper. Nullam hendrerit placerat lorem, sit amet ullamcorper lorem accumsan vel. Nam facilisis dictum fringilla. Aliquam erat volutpat. Nunc efficitur non elit in iaculis.

<!-- tabs:start -->

#### **React**

<!-- panels:start -->
<!-- div:left-panel -->

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tempus nunc nec ipsum convallis, euismod tristique turpis mattis. Aenean elementum condimentum diam quis hendrerit. Fusce facilisis, tortor id ullamcorper eleifend, mauris metus fringilla turpis, sit amet ultrices diam sem non mi. Phasellus eu sodales mi. Curabitur sed tortor vel elit pharetra ullamcorper eu et eros. Vestibulum id pulvinar quam, nec lobortis sapien. Sed posuere dapibus metus, nec convallis justo imperdiet congue. Ut quis lectus sit amet sem imperdiet cursus. Nam id ultricies sapien. Maecenas vel diam tincidunt dui pretium pellentesque et sodales quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse posuere cursus efficitur. Nulla vestibulum vestibulum tellus. Duis aliquam aliquet fringilla. Praesent et turpis at libero condimentum faucibus vitae ac augue.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam et tortor nec justo congue tempus. Pellentesque fermentum consectetur quam quis pellentesque. Nam tempus ultricies nibh, vitae tristique felis consequat nec. Nam vel finibus ipsum. Integer ornare augue a mauris sodales semper. Nullam hendrerit placerat lorem, sit amet ullamcorper lorem accumsan vel. Nam facilisis dictum fringilla. Aliquam erat volutpat. Nunc efficitur non elit in iaculis.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tempus nunc nec ipsum convallis, euismod tristique turpis mattis. Aenean elementum condimentum diam quis hendrerit. Fusce facilisis, tortor id ullamcorper eleifend, mauris metus fringilla turpis, sit amet ultrices diam sem non mi. Phasellus eu sodales mi. Curabitur sed tortor vel elit pharetra ullamcorper eu et eros. Vestibulum id pulvinar quam, nec lobortis sapien. Sed posuere dapibus metus, nec convallis justo imperdiet congue. Ut quis lectus sit amet sem imperdiet cursus. Nam id ultricies sapien. Maecenas vel diam tincidunt dui pretium pellentesque et sodales quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse posuere cursus efficitur. Nulla vestibulum vestibulum tellus. Duis aliquam aliquet fringilla. Praesent et turpis at libero condimentum faucibus vitae ac augue.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam et tortor nec justo congue tempus. Pellentesque fermentum consectetur quam quis pellentesque. Nam tempus ultricies nibh, vitae tristique felis consequat nec. Nam vel finibus ipsum. Integer ornare augue a mauris sodales semper. Nullam hendrerit placerat lorem, sit amet ullamcorper lorem accumsan vel. Nam facilisis dictum fringilla. Aliquam erat volutpat. Nunc efficitur non elit in iaculis.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tempus nunc nec ipsum convallis, euismod tristique turpis mattis. Aenean elementum condimentum diam quis hendrerit. Fusce facilisis, tortor id ullamcorper eleifend, mauris metus fringilla turpis, sit amet ultrices diam sem non mi. Phasellus eu sodales mi. Curabitur sed tortor vel elit pharetra ullamcorper eu et eros. Vestibulum id pulvinar quam, nec lobortis sapien. Sed posuere dapibus metus, nec convallis justo imperdiet congue. Ut quis lectus sit amet sem imperdiet cursus. Nam id ultricies sapien. Maecenas vel diam tincidunt dui pretium pellentesque et sodales quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse posuere cursus efficitur. Nulla vestibulum vestibulum tellus. Duis aliquam aliquet fringilla. Praesent et turpis at libero condimentum faucibus vitae ac augue.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam et tortor nec justo congue tempus. Pellentesque fermentum consectetur quam quis pellentesque. Nam tempus ultricies nibh, vitae tristique felis consequat nec. Nam vel finibus ipsum. Integer ornare augue a mauris sodales semper. Nullam hendrerit placerat lorem, sit amet ullamcorper lorem accumsan vel. Nam facilisis dictum fringilla. Aliquam erat volutpat. Nunc efficitur non elit in iaculis.

<!-- div:right-panel -->

```
Here's some React code
```

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

This part explains about how to do it in Vue.

<!-- div:right-panel -->

```
Here's some Vue code
```

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

This part explains about how to do it in Svelte.

<!-- div:right-panel -->

```
Here's some Svelte code
```

<!-- panels:end -->

<!-- tabs:end -->

## sdfsdf

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tempus nunc nec ipsum convallis, euismod tristique turpis mattis. Aenean elementum condimentum diam quis hendrerit. Fusce facilisis, tortor id ullamcorper eleifend, mauris metus fringilla turpis, sit amet ultrices diam sem non mi. Phasellus eu sodales mi. Curabitur sed tortor vel elit pharetra ullamcorper eu et eros. Vestibulum id pulvinar quam, nec lobortis sapien. Sed posuere dapibus metus, nec convallis justo imperdiet congue. Ut quis lectus sit amet sem imperdiet cursus. Nam id ultricies sapien. Maecenas vel diam tincidunt dui pretium pellentesque et sodales quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse posuere cursus efficitur. Nulla vestibulum vestibulum tellus. Duis aliquam aliquet fringilla. Praesent et turpis at libero condimentum faucibus vitae ac augue.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam et tortor nec justo congue tempus. Pellentesque fermentum consectetur quam quis pellentesque. Nam tempus ultricies nibh, vitae tristique felis consequat nec. Nam vel finibus ipsum. Integer ornare augue a mauris sodales semper. Nullam hendrerit placerat lorem, sit amet ullamcorper lorem accumsan vel. Nam facilisis dictum fringilla. Aliquam erat volutpat. Nunc efficitur non elit in iaculis.

<!-- tabs:start -->

#### **React**

<!-- panels:start -->
<!-- div:left-panel -->

asd

<!-- div:right-panel -->

<iframe src="https://stackblitz.com/edit/vitejs-vite-xdygyw?embed=1&file=README.md"></iframe>

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

This part explains about how to do it in Vue.

<!-- div:right-panel -->

```
Here's some Vue code
```

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

This part explains about how to do it in Svelte.

<!-- div:right-panel -->

```
Here's some Svelte code
```

<!-- panels:end -->

<!-- tabs:end -->
