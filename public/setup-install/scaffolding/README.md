# Scaffolding

As you start a new project, there are many decisions and setups you will eventually encounter as you build it out, such as:

- What language feature set to use (e.g. Javascript or Typescript)
- What build tools to use (e.g Webpack, Rollup, ESBuild)
- What code standards to follow, and what formatters and linters to use (e.g. ESLint, Prettier)
- What extensions to integrate into the application (e.g. state management, routing, styling)
- What development tools to set up (e.g. auto-reloading preview servers, component documentation generators)
- What license to apply for the project (e.g. Apache 2.0, MIT)

Trying to make a decision on all of the above and more may be daunting when the project hasn't even started. However, if the decision is left too late, it may make it difficult to integrate into existing code as there may have been coding decisions made that causes conflicts. Rather than starting wholly from scratch, it may be preferable to use **project templates** to create a base template to extend from.

The term "**scaffolding**" generally refers to the activity of generating files and folder structures for a new application, based on project templates. These templates typically incorporate best practices and varied examples, so that developers can simply copy the pattern to suit their needs rather than having to learn how to use those features from scratch.

There are different ways in which a new project can be scaffolded, such as cloning/forking a template Git repository, or running a project generator CLI. While scaffolding from Git projects allow for a wide range of different project structures, a CLI typically allows for a greater amount of control over the scaffolding process as it can offer multiple options for the developer to choose from, and it will alter the resulting project structure based on what was chosen.

For the purposes of this document, we will be focusing on the latter.

## Framework scaffolding CLI

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

The most commonly used scaffolding CLI for React is the [Create React App](https://create-react-app.dev/) package - this creates various React projects based on templates.

All custom scaffolding behaviours are enabled by templates only, which means that there isn't a way to have an interactive "wizard" for choosing between various options.

Running the CLI with the folder name will initialise the project with a base template.

```terminal
$|npm install -g create-react-app
$|create-react-app ./react-project
$|# (or simply run "npm create react-app ./react-project")
```

```
Creating a new React app in /../react-project.

Installing packages. This might take a couple of minutes.
Installing react, react-dom, and react-scripts with cra-template...
< npm output >

Initialized a git repository.

Installing template dependencies using npm...
< npm output >

Removing template package using npm...
< npm output >

Created git commit.

Success! Created react-project at /../react-project
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd react-project
  npm start

Happy hacking!
```

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->

## Yeoman generators

[Yeoman](https://yeoman.io/) is a scaffolding tool written in Node.js that allows for easy ways to generate a base project. These are based on "generators" which can define template folder structures, user inputs for interactive scaffolding and more to generate a base project of any type.

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

There are currently no definitive Yeoman generators for a React-only project that are actively being maintained. There are, however, quite a few generators that incorporate React into a larger project (such as [jhipster](https://github.com/jhipster/generator-jhipster) that combines Spring and React/Angular/Vue).

Yeoman generators for React can be searched on [NPM](https://www.npmjs.com/search?q=yeoman%20react&ranking=maintenance).

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->
