# Buildless deployment

All web UI frameworks are expected to run on the browser by default. While there are many ways in which we can set up the application to include them, the simplest and most lightweight way to use them within your webpages is to directly import the framework runtime to the browser.

This is referred to as **buildless deployment**, because you do not require a server to first "bundle" or "compile" your web UI framework code before they can be used in the browser.

Typically buildless deployments work through the use of `<script>` tags to download the framework from common **Content-Delivery Networks (CDN)**, such as [jsdelivr](https://www.jsdelivr.com/) and [unpkg](https://www.unpkg.com/). These CDNs host the UI framework scripts for browser clients to load, after which it can start handling the webpage.

Buildless deployments are useful for **"sprinkling in" interactive elements** to your site. If you wish to add more dynamic and interactive elements to only certain sections of the site, it may be overkill to revamp the whole site to use a web UI framework. Instead, you can include the UI framework and component scripts through HTML, then load them into specific elements on the page as needed - this is a lot more lightweight than having to set up a build process.

It can also allow for **gradual migration** of existing web applications. If an application uses some other web technology (such as server-side templating or PHP), it's expected to have a build process that is specifically tailored to that technology - if a framework allows for buildless deployments, the application can simply include the UI framework through the base HTML templates, and start using the framework on certain parts or pages. This can be extended gradually to cover the whole web application.

The downside to this is that **buildless deployments are not scalable**. Many development practices and project structuring necessitates some sort of build step to the pipeline, including:

- **Using multiple files to split the code into modules** - as the project grows even slightly larger, it's far more beneficial to split the codebase into different files to group similar responsibilities together. With buildless deployment, **you need to import each and every file manually**, which will quickly become tedious.<br/>Note that if you are targeting browsers that has [ECMAScript modules (ESM)](https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/) support, then this will become easier to handle as you can import other files directly from `<script>` tags.

- **Using non-standard syntax and features** - a framework can often expect developers to adopt certain programming patterns in order to make using the framework easier. If these patterns are not directly understood by web browsers, **a build step to translate them into pure Javascript is required**.

- **Optimising your script size** - because Javascript is always requested as a network resource, a client has to fetch them via network requests. In order to optimise the size of your script to make them load quicker, many build tools offer a "minification" process to reduce the file size. This is **only available through the use of build tools**, so buildless deployments may not benefit from this.<br/>It is important to note that most web UI frameworks available on CDNs are already minified, so the framework import is typically already optimised.

In essence, by using buildless deployment you will miss out on all the benefits of build tools, in exchange for an extremely quick and lightweight way to start using the framework on the browser.

## Importing into the browser

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

?> For more detailed information, refer to the [React documentation](https://react.dev/learn/add-react-to-an-existing-project).

<!-- panels:start -->
<!-- div:left-panel -->

React supports buildless deployments out of the box - when imported from a CDN, it makes available methods to create a React "root node", under which you can add React components and handle reactivity.

On browsers, `react` and `react-dom` must both be imported. The former handles the core React functionalities such as hooks and props, while the latter connects those functionalities to the browser's DOM (through a React-managed "virtual DOM"). The reason why these are separated boils down to the React team wanting to separate the core React concepts (components, states, declarative rendering, and more) from the "manifestation" of those concepts, in our case the browser's DOM. You can read about this on their [blog post](https://legacy.reactjs.org/blog/2015/07/03/react-v0.14-beta-1.html).

These two bundles have a combined total size of around:

- **Unpacked**: 10.74kB + 131.88kB = 142.62kB
- **gzip compressed**: 5.01kB + 43.82kB = 48.83kB
- **brotli compressed**: 4.78kB + 38.26kB = 43.04kB

!> Different CDNs will have different compression ratios. These values are based on the smallest size seen across several CDNs.

Once imported, the browser can access methods by the `React` and `ReactDOM` globals.

<!-- div:right-panel -->

```html
<div id="app"></div>

<!-- Bottom of <body> -->
<script src="https://cdn.jsdelivr.net/npm/react@latest/umd/react.production.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/react-dom@latest/umd/react-dom.production.min.js"></script>
<script>
  const element = React.createElement("h1", {}, "Hello, world!");
  const root = ReactDOM.createRoot(document.getElementById("app"));
  root.render(element);
</script>
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

Alternatively for modern browsers, React can be imported through ESM. This has the benefits of being able to use a more modern module-based importing, which means you do not require any `<sciprt>` imports alongside the main script.

<!-- div:right-panel -->

```html
<div id="app"></div>

<!-- Bottom of <body> -->
<script type="module">
  import React from "https://esm.sh/react@18.2.0";
  import ReactDOM from "https://esm.sh/react-dom@18.2.0";

  const element = React.createElement("h1", {}, "Hello, world!");
  const root = ReactDOM.createRoot(document.getElementById("app"));
  root.render(element);
</script>
```

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->
