# Build tools and compilation

In software development, we often want to develop software in ways that are easier to manage, especially if there are multiple developers that are working on the same codebase. For example, the source code would be written in a language that is more easily understood and parsable by developers, but end-users cannot run them directly without having them converted into either an executable file or a deployed application.

In order to transform your source code (usually split into many files) and its library dependencies into something the end user can use, we have to "build" the package into an optimised distributable code. This is commonly known as **compiling** the source code.

In Javascript, there are a number of key concepts surrounding the build process:

- **compiling** - the process of taking all of the developer-written source code and dependencies (which may use a module system only applicable for development), and transform them into a format that can run on the browser.<br/>This is also referred to as "transpiling", because both the source code and the resulting output are in Javascript (or some _superset_ of Javascript that has richer syntax and features). Some transpilation process require extensions, as new Javascript syntax are proposed by different web UI frameworks.

- **bundling** - the process of taking all of the compiled code and producing one chunk (or a few chunks) of Javascript and auxiliary files (such as CSS and JSON). After your code has been bundled, it can be viewed through the outputted HTML that has been set up to import the Javascript chunk and related files.<br/>The purpose of bundling is to reduce the number of requests a browser has to make in order to load all of the Javascript required to run the application - without this process the browser would need to make several requests to get all of the compiled files.

- **minifying** - the process of reducing the bundled file size by removing unnecessary code and stripping down on unnecessary characters. The resulting output will functionally be identical to what was there before, but with variable names reduced to only a few characters, stripping spaces and newlines, and other optimisations applied.<br/>By minifying code, the application can be loaded faster by the browser since the file size is smaller.

A build tool's job is to enable these activities, and allow for a better development experience by providing ways to preview the bundled code and customise the compilation process. The build tools mentioned in this document are installed and ran through [Node.js](https://nodejs.org/en) and [Node Package Manager (NPM)](https://www.npmjs.com/), though some bundler's underlying runtime may be different.

## Using Webpack

<span class="date">As of: April 2023</span>

[Webpack](https://webpack.js.org/) is a standard Javascript bundler: it is one of the more advanced and mature bundlers avaialble that supports several useful features, such as [hot module replacement](https://webpack.js.org/concepts/hot-module-replacement/) and [module federation](https://webpack.js.org/concepts/module-federation/).

At its core, Webpack requires two packages to be installed - `webpack` and `webpack-cli`. Without the latter, it can be difficult to configure webpack to bundle the code as you would need to manually invoke compilation steps through custom scripts or command-lines.

```terminal
$|npm install -D webpack webpack-cli
```

Webpack makes heavy use of [Babel](https://babeljs.io/), a Javascript compiler/transpiler that can be extended through plugins and presets. Most UI frameworks require Babel to be set up properly, as well as a "loader" to be configured so that Webpack knows to defer to Babel to resolve custom syntax and add polyfills first before bundling the Javascript:

```terminal
$|npm install -D @babel/core babel-loader
```

The bundling process is typically configured through a configuration file, which can be passed into the Webpack CLI. For each of the web UI frameworks, the same base configuration can be used with minor tweaks:

```js
// webpack.config.js - this file uses CommonJS import/export patterns.
// NOTE: This file *must* be in CommonJS format, as Webpack only supports absoltue paths, and __dirname is not available in ESM.

// Node.js library to manipulate file paths
const path = require("path");
// Plugin to generate a base HTML, injected with a <script> tag that references the bundle.
const HtmlWebpackPlugin = require("html-webpack-plugin");

// The configuration object will be "exported" from this file, making it importable by Webpack.
// For more information, refer to the Webpack documentation: https://webpack.js.org/configuration/
module.exports = {
  mode: "production", // "development" allows for debug features, otherwise "production" should be default.
  entry: "./src/index.js", // Each project should have an "entrypoint" that sets up the application.
  output: {
    filename: "main.js", // Name of the main bundle to generate.
    path: path.resolve(__dirname, "dist"), // Where to place the generated bundle - in our case, a "dist" folder on the same level as this config file.
  },
  // Configures various options for the bundling process
  module: {
    // Set of "rules" to apply based on conditions
    rules: [
      {
        test: /\.jsx?$/, // For files ending with .js or .jsx,
        exclude: [/node_modules/, /.yarn/], // But not in these directories,
        use: {
          loader: "babel-loader", // Use Babel to "load" the file for bundling, i.e. apply Babel transformations.
        },
      },
    ],
  },
  plugins: [
    // List of extra steps to apply during the bundling process
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "index.template.html"), // Use this HTML template for generating the base HTML.
    }),
  ],
};
```

```html
<!-- index.template.html -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>My App</title>
  </head>
  <body>
    <!-- This div is where the UI framework will get loaded into -->
    <div id="app"></div>
  </body>
</html>
```

Running the Webpack CLI command to bundle the project will lead to three files being created:

- `index.html` - the HTML page based on the provided template, with a `<script>` tag to import the main Javascript bundle
- `main.js` - the main Javascript bundle which contains all of the code, including used dependencies, transpiled to run on the browser.
- `main.js.LICENSE.txt` - a compilation of license texts present in all of the dependencies used

```terminal
$|webpack build --config webpack.config.js
```

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

<!-- panels:start -->
<!-- div:left-panel -->

React utilises a special syntax for writing components called [JSX](https://react.dev/learn/writing-markup-with-jsx) (for more information, refer to the [Components](/basic/components/) page).

<!-- div:right-panel -->

```terminal
$|npm install react react-dom
```

```js
// src/index.js

import React from "react";
import ReactDOM from "react-dom";

function App() {
  return <h1>Hello, world!</h1>;
}

ReactDOM.render(<App />, container);
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

In order to use this, we need to configure Babel to use the React preset. This can be installed through NPM, and configured by creating a file on the root directory named `babel.config.js`.

<!-- div:right-panel -->

```terminal
$|npm install -D @babel/preset-react
```

```js
// babel.config.js

module.exports = {
  presets: ["@babel/preset-react"],
};
```

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->

## Using Rollup

<span class="date">As of: April 2023</span>

[Rollup](https://rollupjs.org/) is a Javascript bundler that heavily utilises plugins to customise many different aspects of the bundling behaviour. As a result, it is a very extensible and modular build tool which can adapt to a lot of different use cases.

One of the features Rollup offers is [tree-shaking](https://rollupjs.org/introduction/#tree-shaking), which analyses which exact submodules (read: functions and classes) are being used, and only bundle the ones that are actively being used. The resuling bundle is expected to be functionally the same, but with extra methods and subroutines removed to shave file size down.

Rollup can be installed by installing the `rollup` NPM package - the CLI comes out-of-the-box, so we do not need to install a separate package for it.

```terminal
$|npm install -D rollup
```

Almost every bundling behaviour is controlled by Rollup plugins. Just as we did for Webpack, we can configure Babel to resolve custom syntax and add polyfills first before bundling the Javascript - but in addition, we want to install other plugins that will ensure that the generated bundle can be used standalone:

```terminal
$|npm install -D @babel/core @rollup/plugin-babel @rollup/plugin-commonjs @rollup/plugin-node-resolve rollup-plugin-generate-html-template
```

The bundling process is typically configured through a configuration file, which can be passed into the Rollup CLI. For each of the web UI frameworks, the same base configuration can be used with minor tweaks:

```js
// rollup.config.js - this file uses ESM import/export patterns.

// Plugin to apply Babel transformations
import babel from "@rollup/plugin-babel";
// Plugin to convert CommonJS modules to ESM - required to make majority of NPM-installed dependencies work on the browser
import commonjs from "@rollup/plugin-commonjs";
// Plugin to include dependencies into the bundle - by default, only your code is bundled and the rest are considered "external"
import resolve from "@rollup/plugin-node-resolve";
// Plugin to generate a <script>-injected HTML, based on a template
import html from "rollup-plugin-generate-html-template";

// The configuration object will be "exported" from this file, making it importable by Rollup.
// For more information, refer to the Rollup documentation: https://rollupjs.org/configuration-options
export default {
  input: "./src/index.js", // Each project should have an "entrypoint" that sets up the application.
  output: {
    file: "dist/main.js", // Name and directory of the main bundle to generate.
  },
  // Configures various options for the bundling process. The order of these plugins matter, as they are applied in array order.
  plugins: [
    babel(), // First transpile the project code
    commonjs(), // Then convert dependencies to ESM
    resolve(), // Then include all used dependencies into the bundle
    html(
      // And finally generate a HTML that includes the bundle through <script>
      {
        template: "index.template.html", // Use this HTML template for generating the base HTML.
        target: "index.html", // Name of the resulting HTML
      }
    ),
  ],
};
```

?> The same HTML template from the Webpack example is used.

Running the Rollup CLI command to bundle the project will lead to two files being created:

- `index.html` - the HTML page based on the provided template, with a `<script>` tag to import the main Javascript bundle
- `main.js` - the main Javascript bundle which contains all of the code, including used dependencies, transpiled to run on the browser.

```terminal
$|rollup --config rollup.config.js
```

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

<!-- panels:start -->
<!-- div:left-panel -->

The setup required for React is more or less the same as what's required for Webpack. React utilises a special syntax for writing components called [JSX](https://react.dev/learn/writing-markup-with-jsx) (for more information, refer to the [Components](/basic/components/) page).

<!-- div:right-panel -->

```terminal
$|npm install react react-dom
```

```js
// src/index.js

import React from "react";
import ReactDOM from "react-dom";

function App() {
  return <h1>Hello, world!</h1>;
}

ReactDOM.render(<App />, container);
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

In order to use this, we need to configure Babel to use the React preset. This can be installed through NPM, and configured by creating a file on the root directory named `babel.config.js`.

<!-- div:right-panel -->

```terminal
$|npm install -D @babel/preset-react
```

```js
// babel.config.js

module.exports = {
  presets: ["@babel/preset-react"],
};
```

<!-- panels:end -->

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->

## Using ESBuild

<span class="date">As of: April 2023</span>

[ESBuild](https://esbuild.github.io/) is an extremely performant bundler for Javascript, written in [Go](https://go.dev/). While it is not as mature as other bundlers, the peformance of ESBuild is magnitudes faster than Webpack, Rollup, or any other bundlers that are written in Node.js.

ESBuild can be installed by installing the `esbuild` NPM package - the CLI comes out-of-the-box, so we do not need to install a separate package for it. Note that this will install what's called a "_native executable_", which means that the ESBuild program is tailored to the exact OS that it is being installed on.

```terminal
$|npm install -D esbuild
```

Unlike Webpack or Rollup, ESBuild typically does not rely on Babel and instead does Javascript transpilation using its own built-in transpiler. Because this is written in Go with performance in mind, it allows ESBuild to be extremely fast at transpiling and bundling Javascript. This also means that **existing Babel plugins are not supported by ESBuild**, unless you install a plugin to intentionally use Babel for some of the transpilation process.

The bundling process is typically configured through passing in CLI flags - in general, there isn't a configuration file you can create and pass into the CLI. On the other hand, when plugins are required for some projects, the **Javascript API for ESBuild needs be used** to configure them into the bundling process.

<!-- tabs:start -->

#### **React**

<span class="date">As of: April 2023</span>

<!-- panels:start -->
<!-- div:left-panel -->

As mentioned previously, React uses a special syntax called [JSX](https://react.dev/learn/writing-markup-with-jsx) to write components. ESBuild has built-in support for JSX, and so it is able to transpile them into Javascript without needing to set up Babel or requiring any additional plugins.

<!-- div:right-panel -->

```terminal
$|npm install react react-dom
```

```js
// src/index.js

import React from "react";
import ReactDOM from "react-dom";

function App() {
  return <h1>Hello, world!</h1>;
}

ReactDOM.render(<App />, container);
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

Compiling the component code is done by passing in the appropriate CLI arguments. Similar to Rollup, ESBuild by default does not bundle the dependencies into the output file. You must pass in a `--bundle` flag to enable this behaviour.

Also similar to Webpack, ESBuild has the concept of "[loaders](https://esbuild.github.io/content-types/)" to handle different file types. By default, a `.js` file is loaded by a `js` loader which does not understand JSX - however, we can tell ESBuild to use a `jsx` loader for `.js` files by passing in the `--loader:.js=jsx` flag.

<!-- div:right-panel -->

```terminal
$|esbuild src/index.js --bundle --loader:.js=jsx --outfile=dist/main.js
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

Generating the HTML can only be done through the use of plugins (such as [`@craftamap/esbuild-plugin-html`](https://github.com/craftamap/esbuild-plugin-html)). This specification cannot be done through the use of CLI, and must be set up using the Javascript API.

<!-- div:right-panel -->

```terminal
$|npm install -D @craftamap/esbuild-plugin-html
$|node esbuild.js
```

```js
// esbuild.js

// Javascript API for ESBuild
import esbuild from "esbuild";
// One example of an ESBuild HTML plugin
import { htmlPlugin } from "@craftamap/esbuild-plugin-html";

// Programmatically start the build process with configured options
await esbuild.build({
  // Each project should have an "entrypoint" that sets up the application.
  entryPoints: ["src/index.js"],
  // Output directory of all files
  outdir: "dist",
  // Use the JSX loader for .js files
  loader: {
    ".js": "jsx",
  },
  // Enable dependency bundling
  bundle: true,
  // Required for the HTML plugin - generates a file detailing the build metadata
  metafile: true,
  // Use the HTML plugin during the build process
  plugins: [
    htmlPlugin({
      // Plugin expects a list of HTML files to generate
      files: [
        {
          // The name of the output HTML file
          filename: "index.html",
          // This HTML will be based on this Javascript file - multiple files are allowed to be included
          entryPoints: ["src/index.js"],
          // The template HTML to use for the generated HTML
          htmlTemplate: `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>My App</title>
  </head>
  <body>
    <div id="app"></div>
  </body>
</html>`,
        },
      ],
    }),
  ],
});
```

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->
