# Entrypoint

Typically, a UI framework is built on Javascript to provide features such as reactivity, DOM abstraction, and component management. As such, there must be some way for that Javascript to set itself up and execute - a sort of "entrypoint" exists for all UI frameworks.

<!-- tabs:start -->

#### **React**

<span class="date">As of: February 2024</span>

?> For more detailed information, refer to the [React documentation](https://react.dev/learn/add-react-to-an-existing-project).

<!-- panels:start -->
<!-- div:left-panel -->

React is architected to have *separate libraries* for the core React logic and the actual "rendering" of said logic. For browsers the library to use is [react-dom](https://www.npmjs.com/package/react-dom) (in particular the `/client` modules), while for other platforms there may be other libraries that act as the entrypoint for the React logic.

`ReactDOM.createRoot` will create a `Root` object that targets a specific DOM element, with methods to `render` and `unmount` React's managed hierarchy of components.

<!-- div:right-panel -->
```js
import React from 'react';
import ReactDOM from 'react-dom/client';

const appRoot = ReactDOM.createRoot(document.getElementById('root'));
appRoot.render(
  // In projects backed with build tools, this would usually be JSX
  React.createElement(/* ... */)
);
```

<!-- panels:end -->

#### **Vue**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

#### **Svelte**

<!-- panels:start -->
<!-- div:left-panel -->

(Future content)

<!-- panels:end -->

<!-- tabs:end -->