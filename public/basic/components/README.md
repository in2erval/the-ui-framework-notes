# Components


<!-- tabs:start -->

#### **React**

<!-- panels:start -->
<!-- div:left-panel -->

React primarily uses a set of syntax for describing component structure within Javascript called [JSX](https://react.dev/learn/writing-markup-with-jsx). JSX can be thought of as a hybrid of HTML and Javascript, where you can create nested element structures (akin to HTML) and treat them as Javascript objects.

<!-- div:right-panel -->

```js
// src/App.js

import React from "react";

function App() {
  return <h1>Hello, world!</h1>;
}

export default App;
```

<!-- panels:end -->

<!-- panels:start -->
<!-- div:left-panel -->

This special syntax is not a feature of regular Javascript, and therefore must be transpiled before it can run on the browser.

<!-- div:right-panel -->

```js

```

<!-- panels:end -->

<!-- tabs:end -->